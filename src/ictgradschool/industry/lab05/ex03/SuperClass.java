package ictgradschool.industry.lab05.ex03;

public class SuperClass {
    public int x = 10;
    static int y = 10;

    SuperClass() {
        x = y++; //x = y, y = y+1
    }

    public int foo() {
        return x;
    }

    public static int goo() {
        return y;
    }
}