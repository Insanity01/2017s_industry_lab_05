package ictgradschool.industry.lab05.quizex02;

/**
 * Represents a horse.
 *
 * TODO Make this implement IAnimal and IFamous, and provide appropriate implementations of those methods.
 */

public class Horse extends Animal implements IFamous {

    @Override
    public String sayHello() {

        return myName() + " says " + "neigh.";
    }

//    @Override
//    public boolean isMammal() {
//        return true;
//
//    }

    @Override
    public String myName() {

        return "Mr Ed.";
    }

//    @Override
//    public int legCount() {
//        return 4;
//    }

    @Override
    public String famous() {
        return "Phar Lap";
    }

}
