package ictgradschool.industry.lab05.quizex02;

/**
 * Represents something famous
 */
public interface IFamous {

    /**
     * Gets the name of a famous member of the species.
     */
    public String famous();

}
