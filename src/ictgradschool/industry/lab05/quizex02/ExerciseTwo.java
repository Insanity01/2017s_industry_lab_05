package ictgradschool.industry.lab05.quizex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        Animal[] animals = new Animal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Dog();
        animals[1] = new Bird();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(Animal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.

        for (int i = 0; i < list.length; i++) {

            System.out.println(list[i].sayHello());

            if (list[i].isMammal()) {
                System.out.println(list[i].myName() + " is a mammal.");
            } else {
                System.out.println(list[i].myName() + " is a non-mammal.");
            }

            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs.");

            // TODO If the animal also implements IFamous, print out that corresponding info too.

            if (list[i] instanceof Horse) {
                Horse other = (Horse) list[i];
                System.out.println("This is a famous name of my animal type: " + other.famous());
            }

            System.out.println("--------------------------------------------------------------");
        }
    }

    public static void main(String[] args) {

        new ExerciseTwo().start();
    }
}
