package ictgradschool.industry.lab05.quizex02;

/**
 * Represents a Bird.
 * <p>
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird extends Animal {

    @Override
    public String sayHello() {

        return myName() + " says " + "tweet tweet.";
    }

//    @Override
//    public boolean isMammal() {
//        return false;
//    }

    @Override
    public String myName() {

        return "Tweety the Bird";
    }

//    @Override
//    public int legCount() {
//        return 2;
//    }
}





