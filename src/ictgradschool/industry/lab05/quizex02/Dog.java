package ictgradschool.industry.lab05.quizex02;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog extends Animal {

    @Override
    public String sayHello() {

        return myName() + " says " +   "woof woof.";
    }

//    @Override
//    public boolean isMammal() {
//        return true;
//
//    }

    @Override
    public String myName() {

        return "Bruno the dog";
    }
//
//    @Override
//    public int legCount() {
//        return 4;
//    }


}
