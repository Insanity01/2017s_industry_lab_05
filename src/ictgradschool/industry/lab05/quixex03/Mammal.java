package ictgradschool.industry.lab05.quixex03;

/**
 * Created by vwen239 on 20/11/2017.
 */
public class Mammal {

    public int legs = 4;
    public String myName = "Animal";

    public void myIntroduction() {
        System.out.println("Hello I am " + myName + ", an Animal");
        System.out.println("I have " + legs + " legs.");
    }

    public String myName() {
        return myName;
    }

}
