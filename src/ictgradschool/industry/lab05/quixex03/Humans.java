package ictgradschool.industry.lab05.quixex03;

/**
 * Created by vwen239 on 20/11/2017.
 */
public class Humans extends Mammal implements Herbivore, Carnivore {

    public Humans (){
        this.legs = 2;
    }

    public void myIntroduction() {
        System.out.println("I am " + myName());
        System.out.println("I have " + this.legs + " legs");
        System.out.println("I eat " + kgPlantsDaily() + "kg of plants a day");
        System.out.println("I eat " + kgMeatDaily() + "kg of meat a day");
        System.out.println("----------------------------");
    }

    public int kgPlantsDaily() {
        return 1;
    }

    public int kgMeatDaily() {

        return 1;
    }

    public String myName() {
        return "Vincent Wen the Human";
    }
}
