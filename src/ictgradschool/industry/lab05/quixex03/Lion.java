package ictgradschool.industry.lab05.quixex03;

/**
 * Created by vwen239 on 20/11/2017.
 */
public class Lion extends Mammal implements Carnivore {

    public void myIntroduction() {
        System.out.println("I am " + myName());
        System.out.println("I have " + legs + " legs");
        System.out.println("I eat " + kgMeatDaily() + "kg of meat a day");
        System.out.println("----------------------------");
    }

    public int kgMeatDaily() {

        return 20;
    }

    public String myName() {
        return "Simba the Lion";
    }
}



