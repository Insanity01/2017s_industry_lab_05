package ictgradschool.industry.lab05.quixex03;

/**
 * Created by vwen239 on 20/11/2017.
 */
public class Giraffe extends Mammal implements Herbivore {

    public void myIntroduction() {
        System.out.println("I am " + myName());
        System.out.println("I have " + legs + " legs.");
        System.out.println("I eat " + kgPlantsDaily() + "kg of plants a day");
        System.out.println("----------------------------");
    }

    public int kgPlantsDaily() {
        return 25;
    }

    public String myName() {
        return "Geoffrey the Giraffe";
    }


}
