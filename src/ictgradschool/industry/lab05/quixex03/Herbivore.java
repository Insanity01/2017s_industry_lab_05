package ictgradschool.industry.lab05.quixex03;

/**
 * Created by vwen239 on 20/11/2017.
 */
public interface Herbivore {
    int kgPlantsDaily();
}
