package ictgradschool.industry.lab05.quixex03;

public class Program {

    public void start() {
        Mammal[] mammals = new Mammal[3];

        mammals[0] = new Humans();
        mammals[1] = new Lion();
        mammals[2] = new Giraffe();

        printMammalIntroduction(mammals);
    }

    public static void main(String[] args) {
        new Program().start();
    }

    public void printMammalIntroduction(Mammal[] mammals) {

        for (int i = 0; i < mammals.length ; i++){
            mammals[i].myIntroduction();
        }

    }
}
